from easygui import *

def main():
    people = integerbox(msg="How many people are there?", title="Loops, how are you")
    for i in range(people):
        feeling = enterbox(msg="How are you?", title="Loops, how are you?")
        msgbox(msg="You are feeling, " + feeling, title="Loops, how are you?")
    msgbox("BYE!!")
        
main()